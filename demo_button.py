from exduir import *
from ctypes import *


@ctypes.PYFUNCTYPE(c_longlong, c_int, c_int, c_int, c_size_t, c_size_t)
def on_button_event(hobj: int, id: int, code: int, wparam: int, lparam: int) -> int:
    if code == NM_CLICK:
        if hobj == hobj_button1:
            ex.objEnable(hobj_button1, False)
            ex.objSetPadding(hobj_button1, 0, 20, 5, 5, 5, True)
        elif hobj == hobj_button2:
            ex.objEnable(hobj_button1, True)
        elif hobj == hobj_button3:
            ex.objSetText(hobj_button3, '自身文本被改动', True)
        elif hobj == hobj_button4:
            str1 = ex.objGetText(hobj_button1)
            ex.objSetText(hobj_button4, '按钮1文本'+str1, True)
    elif code == NM_CUSTOMDRAW:
        if hobj == hobj_button5:
            ps = ex.castPaintStruct(lparam)
            if (ps.state & STATE_DOWN) != 0:
                crbkg = ex.RGB2ARGB(255, 51)
            elif (ps.state & STATE_HOVER) != 0:
                crbkg = ex.RGB2ARGB(16754943, 51)
            else:
                crbkg = ex.RGB2ARGB(16777215, 51)
            brush = ex.brushCreate(crbkg)
            ex.canvasFillRect(ps.hcanvas, brush, 0, 0,
                              ps.rcpaint_right, ps.rcpaint_bottom)
            ex.brushDestroy(brush)
    elif code == NM_CHECK:
        if hobj == hobj_switch2:
            if wparam!=0:
                ex.messageBox(hobj, '开启', '取开关状态', 128, EMBF_CENTEWINDOW)
            else:
                ex.messageBox(hobj, '关闭', '取开关状态', 48, EMBF_CENTEWINDOW)
    return 0


if __name__ == '__main__':
    ex = ExDUIR()
    hwnd = ex.wndCreate(0, 'demo button', 0, 0, 400, 300)
    hexdui = ex.duiBindWindowEx(hwnd, EWS_MAINWINDOW | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_BUTTON_MAX |
                                EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0)
    ex.duiSetLong(hexdui, EWL_CRBKG, ex.RGBA(150, 150, 150, 255))
    hobj_button1 = ex.objCreate('button', '禁用自身', -1, 10, 30, 120, 30, hexdui)
    ex.objHandleEvent(hobj_button1, NM_CLICK, on_button_event)
    hobj_button2 = ex.objCreate(
        'button', '解除按钮1禁用', -1, 10, 70, 120, 30, hexdui)
    ex.objHandleEvent(hobj_button2, NM_CLICK, on_button_event)
    hobj_button3 = ex.objCreate(
        'button', '改动自身文本', -1, 10, 110, 120, 30, hexdui)
    ex.objHandleEvent(hobj_button3, NM_CLICK, on_button_event)
    hobj_button4 = ex.objCreate(
        'button', '取按钮1文本', -1, 10, 150, 120, 30, hexdui)
    ex.objHandleEvent(hobj_button4, NM_CLICK, on_button_event)
    hobj_button5 = ex.objCreateEx(EOS_EX_FOCUSABLE | EOS_EX_CUSTOMDRAW | EOS_EX_COMPOSITED,
                                  'button', '重画按钮1', -1, 150, 30, 120, 30, hexdui, 0, DT_CENTER | DT_VCENTER, 0, 0)
    ex.objHandleEvent(hobj_button5, NM_CUSTOMDRAW, on_button_event)
    hobj_switch = ex.objCreate(
        'switch', '已开启|已关闭', -1, 150, 110, 80, 30, hexdui)
    hobj_switch2 = ex.objCreate(
        'switch', '', -1, 150, 150, 60, 30, hexdui)
    ex.objSendMessage(hobj_switch2, BM_SETCHECK, 1, 0)
    ex.objHandleEvent(hobj_switch2, NM_CHECK, on_button_event)
    ex.duiShowWindow(hexdui)
    ex.wndMsgLoop()
    ex.unInit()

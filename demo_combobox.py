from exduir import *
from ctypes import *


@ctypes.PYFUNCTYPE(c_longlong, c_int, c_int, c_int, c_size_t, c_size_t)
def on_combobox_event(hobj: int, id: int, code: int, wparam: int, lparam: int) -> int:
    if code == CBN_DROPDOWN:
        print('组合框事件_即将弹出列表')
    elif code == CBN_CLOSEUP:
        print('组合框事件_列表被关闭')
    elif code == CBN_SELCHANGE:
        print('组合框事件_选择项被改变', str(combobox_getselect(hobj_combobox1)))
    elif code == CBN_POPUPLISTWINDOW:
        print('组合框事件_弹出列表窗口')
    return 0


@ctypes.PYFUNCTYPE(c_longlong, c_int, c_int, c_int, c_size_t, c_size_t)
def on_button_event(hobj: int, id: int, code: int, wparam: int, lparam: int) -> int:
    if code == NM_CLICK:
        if hobj == hobj_button1:
            combobox_additem(hobj_combobox1, "测试" +
                             str(combobox_getcount(hobj_combobox1)+1))
        elif hobj == hobj_button2:
            combobox_insertitem(hobj_combobox1, 1, '插入项目')
        elif hobj == hobj_button3:
            ex.messageBox(hexdui, ex.objGetText(
                hobj_combobox1), '组合框内容：', 64, 0)
        elif hobj == hobj_button4:
            ex.objSetText(hobj_combobox1, '置内容', True)
        elif hobj == hobj_button5:
            combobox_clear(hobj_combobox1)
        elif hobj == hobj_button6:
            title = combobox_getitemtext(hobj_combobox1, 1)
            ex.messageBox(hexdui, title, '组合框项目文本：', 64, 0)
        elif hobj == hobj_button7:
            combobox_setselect(hobj_combobox1, 1)
        elif hobj == hobj_button8:
            ex.messageBox(hexdui, str(combobox_getitemvalue(
                hobj_combobox1, 1)), '组合框项目数值：', 64, 0)
        elif hobj == hobj_button9:
            combobox_setitemvalue(hobj_combobox1, 1, 100)
        elif hobj == hobj_button10:
            combobox_showlist(hobj_combobox1)
    return 0


def combobox_getcount(hobj):
    return ex.objSendMessage(hobj, 326, 0, 0)  # CB_GETCOUNT


def combobox_insertitem(hobj, index, title):
    ex.objSendMessage(hobj, 330, index, title)  # CB_INSERTSTRING


def combobox_additem(hobj, title):
    ex.objSendMessage(hobj, 323, 0, title)  # CB_ADDSTRING


def combobox_getitemtext(hobj, index):
    text_len = ex.objSendMessage(hobj, 329, index, 0)*2+2  # CB_GETLBTEXTLEN
    ret_str = ''
    ret_str.zfill(text_len)
    ret = ctypes.c_wchar_p(ret_str)
    ex.objSendMessage(hobj, 328, index, ret)  # CB_GETLBTEXT
    return ret.value


def combobox_clear(hobj):
    ex.objSendMessage(hobj, 331, 0, 0)  # CB_RESETCONTENT


def combobox_setselect(hobj, index):
    ex.objSendMessage(hobj, 334, index, 0)  # CB_SETCURSEL


def combobox_getselect(hobj):
    return ex.objSendMessage(hobj, 327, 0, 0)  # CB_GETCURSEL


def combobox_getitemvalue(hobj, index):
    return ex.objSendMessage(hobj, 336, index, 0)  # CB_GETITEMDATA


def combobox_setitemvalue(hobj, index, value):
    return ex.objSendMessage(hobj, 337, index, value)  # CB_SETITEMDATA


def combobox_showlist(hobj):
    ex.objSendMessage(hobj, 335, 1, 0)  # CB_SHOWDROPDOWN


def combobox_setminvisiable(hobj, minvisiable):
    ex.objSendMessage(hobj, 5889, minvisiable, 0)


if __name__ == '__main__':
    ex = ExDUIR()
    hwnd = ex.wndCreate(0, 'demo combobox', 0, 0, 400, 300)
    hexdui = ex.duiBindWindowEx(hwnd, EWS_MAINWINDOW | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_BUTTON_MAX |
                                EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0)
    ex.duiSetLong(hexdui, EWL_CRBKG, ex.RGBA(150, 150, 150, 255))

    hobj_combobox1 = ex.objCreate(
        'combobox', '组合框内容', -1, 20, 50, 120, 28, hexdui)
    ex.objHandleEvent(hobj_combobox1, CBN_DROPDOWN,
                      on_combobox_event)  # 组合框事件_即将弹出列表
    ex.objHandleEvent(hobj_combobox1, CBN_CLOSEUP,
                      on_combobox_event)  # 组合框事件_列表被关闭
    ex.objHandleEvent(hobj_combobox1, CBN_SELCHANGE,
                      on_combobox_event)  # 组合框事件_选择项被改变
    ex.objHandleEvent(hobj_combobox1, CBN_POPUPLISTWINDOW,
                      on_combobox_event)  # 组合框事件_弹出列表窗口

    hobj_button1 = ex.objCreate('button', '添加项目', -1, 160, 50, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button1, NM_CLICK, on_button_event)

    hobj_button2 = ex.objCreate('button', '插入项目', -1, 160, 80, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button2, NM_CLICK, on_button_event)

    hobj_button3 = ex.objCreate('button', '取内容', -1, 160, 110, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button3, NM_CLICK, on_button_event)

    hobj_button4 = ex.objCreate('button', '置内容', -1, 160, 140, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button4, NM_CLICK, on_button_event)

    hobj_button5 = ex.objCreate(
        'button', '清空表项', -1, 160, 170, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button5, NM_CLICK, on_button_event)

    hobj_button6 = ex.objCreate(
        'button', '取项目文本', -1, 270, 50, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button6, NM_CLICK, on_button_event)

    hobj_button7 = ex.objCreate(
        'button', '置现行选择项', -1, 270, 80, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button7, NM_CLICK, on_button_event)

    hobj_button8 = ex.objCreate(
        'button', '取项目数值', -1, 270, 110, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button8, NM_CLICK, on_button_event)

    hobj_button9 = ex.objCreate(
        'button', '置项目数值', -1, 270, 140, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button9, NM_CLICK, on_button_event)

    hobj_button10 = ex.objCreate(
        'button', '弹出列表', -1, 270, 170, 100, 28, hexdui)
    ex.objHandleEvent(hobj_button10, NM_CLICK, on_button_event)

    combobox_setminvisiable(hobj_combobox1, 5)
    ex.duiShowWindow(hexdui)
    ex.wndMsgLoop()
    ex.unInit()

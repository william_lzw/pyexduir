from exduir import *
from ctypes import *
from cef3 import *


@ctypes.PYFUNCTYPE(c_longlong, c_size_t, c_int, c_int, c_size_t, c_size_t, c_void_p)
def msg_proc(hwnd: int, handle: int, umsg: int, wparam: int, lparam: int, lpresult: int) -> int:
    if umsg == 5:
        if hobj_cefbrowser1 != 0:
            ex.objMove(hobj_cefbrowser1, 50, 50, loword(
                lparam)-100, hiword(lparam)-100, False)
    return 0


if __name__ == '__main__':
    ex = ExDUIR()
    hobj_cefbrowser1 = 0
    ex.objCefBrowserInitialize('', True, '', '', 0, 0)
    hwnd = ex.wndCreate(0, 'demo CefBrowser', 0, 0, 800, 600)
    hexdui = ex.duiBindWindowEx(hwnd, EWS_MAINWINDOW | EWS_BUTTON_CLOSE |
                                EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, msg_proc)
    ex.duiSetLong(hexdui, EWL_CRBKG, ex.RGBA(150, 150, 150, 255))
    hobj_cefbrowser1 = ex.objCreate(
        'CefBrowser', '', -1, 30, 30, 750, 550, hexdui)
    ex.objSendMessage(hobj_cefbrowser1, CEFM_LOADURL,
                      0, 'https://www.baidu.com')

    ex.duiShowWindow(hexdui)
    ex.wndMsgLoop()
    ex.unInit()

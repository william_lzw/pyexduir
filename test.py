import cv2
import numpy as np
from exduir import *
from ctypes import *

@ctypes.PYFUNCTYPE(c_longlong, c_size_t, c_int, c_int, c_size_t, c_size_t, c_void_p)
def msg_proc(hwnd: int, handle: int, umsg: int, wparam: int, lparam: int, lpresult: int) -> int:
    # if umsg==5:
    #     print('改变窗口大小消息',umsg)
    return 0


@ctypes.PYFUNCTYPE(c_longlong, c_int, c_int, c_int, c_size_t, c_size_t)
def on_button_event(hobj: int, id: int, code: int, wparam: int, lparam: int) -> int:
    if code == NM_CLICK:
        if hobj == hobj_button1:
            print('1111')
            pass
    return 0

if __name__=='__main__':
    ex = ExDUIR()
    hwnd = ex.wndCreate(0, 'test', 0, 0, 500, 500)
    hexdui = ex.duiBindWindowEx(hwnd, EWS_MAINWINDOW | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_BUTTON_MAX |
                                EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, msg_proc)
    ex.duiSetLong(hexdui, EWL_CRBKG, ex.RGBA(150, 150, 150, 255))

    hobj_button1 = ex.objCreate('button', '按钮1', -1, 10, 50, 100, 30, hexdui)
    ex.objHandleEvent(hobj_button1, NM_CLICK, on_button_event)

    hobj_edit1 = ex.objCreate('edit', '', -1, 110, 50, 200, 30, hexdui)

    img=cv2.imread('res/editbkg.jpg',0)
    img_str=cv2.imencode('.png',img)[1].tobytes()

    hobj_imgbox=ex.objCreate('static','',-1,10,100,100,100,hexdui)
    ex.objSetBackgroundImage(hobj_imgbox,img_str,len(img_str),0,0,BIR_DEFAULT,BIF_DEFAULT,255,True)
    


    ex.duiShowWindow(hexdui)
    ex.wndMsgLoop()
    ex.unInit()

from exduir import *
from ctypes import *


if __name__ == '__main__':
    ex = ExDUIR()
    
    hwnd = ex.wndCreate(0, 'demo datebox', 0, 0, 400, 450)
    hexdui = ex.duiBindWindowEx(hwnd, EWS_MAINWINDOW | EWS_BUTTON_CLOSE|
                                EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0)
    ex.duiSetLong(hexdui, EWL_CRBKG, ex.RGBA(150, 150, 150, 255))
    hobj_cefbrowser1 = ex.objCreate('DateBox', '', -1, 30, 30, 150, 30, hexdui)


    ex.duiShowWindow(hexdui)
    ex.wndMsgLoop()
    ex.unInit()